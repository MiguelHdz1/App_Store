import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  AsyncStorage,
  Text,
  CheckBox,
  View,
  Image,
  ScrollView,
  RefreshControl,
  FlatList,
  TouchableOpacity
} from 'react-native';
import { Switch } from 'react-native-switch';
import { Icon,List, ListItem,Header,Card,Tile} from 'react-native-elements';

export default class interests extends Component{
  static navigationOptions={
    tabBarLabel:'Intereses',
    tabBarIcon:({tintColor})=>(
      <Icon
        name='favorite'
        color='#ffffff' />
    )
  }

  constructor(props){
    super(props)
    this.state={
      data:[],
      refreshing:false,
    }
  }

  componentDidMount=()=>{    
    AsyncStorage.getItem("idUser").then((value) => {
      this.setState({"id": value});
      const {id}=this.state;
      fetch("http://192.168.1.115:8000/api/users/get-interests/"+id)
      .then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({"data":responseJson.interest_user});        
      })
      .catch((error)=>{alert('Error de conexión');});
      });
  }
  _onRefresh(){
    AsyncStorage.getItem("idUser").then((value) => {
      this.setState({"id": value,"refreshing":true});      
      const {id}=this.state;
      fetch("http://192.168.1.115:8000/api/users/get-interests/"+id)
      .then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({"data":responseJson.interest_user,"refreshing":false});
        //alert(this.state.data[0].name);
      })
      .catch((error)=>{
        this.setState({"refreshing":false});
        alert('Error de conexión');
      });
      });
  }


  render() {
    const {navigate} = this.props.navigation;    
    return (
      <View style={{paddingBottom: 55}} >
        <Header
          leftComponent={<TouchableOpacity onPress={() => navigate('info')}><Icon name='info' color='#fff'/></TouchableOpacity>}
          rightComponent={<TouchableOpacity onPress={this.componentDidMount}><Icon name='refresh' color='#fff'/></TouchableOpacity>}
          centerComponent={{ text: 'MIS INTERESES', style: { color: '#fff' } }}
          statusBarProps={{backgroundColor:'#3E5EAB',
          animated:false,
          translucent:false}}
          outerContainerStyles={styles.outerContainer}   
        />
      <ScrollView  
      refreshControl={
        <RefreshControl
        refreshing={this.state.refreshing}
        onRefresh={this._onRefresh.bind(this)}
        progressBackgroundColor='#476dc5'
        colors={['white']}
        />
      }
      >        
      <FlatList      
          data={this.state.data}
          keyExtractor={(x,i)=>i}
          renderItem={
            ({item})=>

          <Tile 
          captionStyle={{paddingBottom:10}}
          imageSrc={{uri: "http://192.168.1.115:8000/images_api/images/"+item.img_url}}
          title={item.name}
          featured
          imageContainer={{flexDirection: 'column',
          		alignItems: 'center',
          		justifyContent: 'center',
          		resizeMode: 'cover',
          		backgroundColor: '#ffffff'
          		}}
          icon={{name: 'favorite',color:'white'}}
          caption="I LIKE IT"
          />
          }
          refreshControl={
            <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh.bind(this)}
            progressBackgroundColor='#476dc5'
            colors={['white']}
            />
          }
        />
      </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  
    outerContainer: {
      position: 'relative',
      backgroundColor: '#476dc5',
      borderBottomColor: '#476dc5',
      borderBottomWidth: 1,
      padding: 12,
      height: 53,
    },
  });