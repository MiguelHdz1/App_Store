import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  AsyncStorage,
  Text,
  CheckBox,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert
} from 'react-native';
import Toast from 'react-native-simple-toast';
import NavigationBar from 'react-native-navbar';
import { Icon,Header,Card,PricingCard } from 'react-native-elements';
import ResponsiveImage from 'react-native-responsive-image';
import PushNotification from 'react-native-push-notification';


export default class productDetail extends Component{

    constructor(props){
        super(props)
        this.state={
          idProduct:'',          
          img_url: "",
          name: "",
          price: "",
          decription: "",
          category: "",                    
          iconSize:26,
          favorite_icon:'crop-square',
          id_user:''
        }
      }

    componentDidMount(){      
        const {state} = this.props.navigation;
        fetch("http://192.168.1.115:8000/api/products/single-product/"+state.params.idProduct)
        .then((response)=>response.json())
        .then((responseJson)=>{
          this.setState({
            "name":responseJson.name,
            "img_url":responseJson.img_url,
            'price':responseJson.price,
            'decription':responseJson.decription,
            'category':responseJson.category            
          });             
        })
        .catch((error)=>{alert('Error de conexión');});
        AsyncStorage.getItem("idUser").then((value) => {         
          fetch("http://192.168.1.115:8000/api/interest/single-user/"+value+"/"+state.params.idProduct)
          .then((response)=>response.json())
          .then((responseJson)=>{    
            if(responseJson.result===true){
              this.setState({"favorite_icon":'favorite'});
              this.setState({"iconSize":30});
            }else{
              this.setState({"favorite_icon":'favorite-border'});
              this.setState({"iconSize":26});
            }                  
          })
          .catch((error)=>{alert('Error de conexión');});
          fetch("http://192.168.1.115:8000/api/products/product-searched/"+value+"/"+state.params.idProduct)
          .then((response)=>response.json())
          .then((responseJson)=>{
            if(responseJson.result===true){
              PushNotification.localNotification({
                title:'Sugerencia de producto',
                message:"Quizá puedes buscar "+ '"'+responseJson.suggestion.name+ '"'
              });
            }
                  
          })
          .catch((error)=>{alert('Error de conexión');});
        });
        
    }

    interestCheck=()=>{
      const {state} = this.props.navigation;
      
      AsyncStorage.getItem("idUser").then((value) => {
        this.setState({'idProduct':state.params.idProduct,'id_user':value});         
        fetch('http://192.168.1.115:8000/api/users/interests',
        {
          method:'POST',
          mode: 'cors',
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            id: value,
            id_product: state.params.idProduct
          })
        }).then((response)=>response.json()).then((responseJson)=>{
          if(responseJson.result===true){
            this.setState({"favorite_icon":'favorite'});
            this.setState({"iconSize":30});
          }else{
            this.setState({"favorite_icon":'favorite-border'});
            this.setState({"iconSize":26});
          }
        }).catch((error)=>{
        alert('Error de conexión');
      });
      });
    }

    buyElement=()=>{
      const {state} = this.props.navigation;
      AsyncStorage.getItem("idUser").then((value) => {
        Alert.alert("Comprar Producto",'Estás apunto de comprar este producto ¿Deseas continuar con la compra?',
        [ 
          {text:'Cancelar',onPress: () =>null,style:'cancel'},
          {text: 'Continuar', onPress: () => {
            fetch('http://192.168.1.115:8000/api/products/buy-product',
            {
              method:'POST',
              mode: 'cors',
              headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
              },
              body: JSON.stringify({
                id_user: value,
                id_product: state.params.idProduct
              })
            }).then((response)=>response.json()).then((responseJson)=>{
              if(responseJson.result===true){
                Toast.showWithGravity('Compra realizada con éxito', Toast.SHORT, Toast.CENTER);
              }
            }).catch((error)=>{
            alert('Error de conexión');
          });
          }}
        ],
        { cancelable: false }
      );
      });
      
    }
  render() {   
      
   const {goBack} = this.props.navigation;
    return (
      
      <View style={{paddingBottom: 60}} >
        <Header
      leftComponent={<TouchableOpacity onPress={() => goBack()}><Icon name='arrow-back' color='#FFFFFF'/></TouchableOpacity>}        
      centerComponent={{ text: this.state.name, style: { color: '#fff' } }}
      rightComponent={<TouchableOpacity onPress={this.interestCheck}><Icon color='#FFFFFF' size={this.state.iconSize} name={this.state.favorite_icon}/></TouchableOpacity>}
      statusBarProps={{backgroundColor:'#3E5EAB'}}
      outerContainerStyles={styles.outerContainer}          
    />
        <ScrollView>         
        <View >
      <ResponsiveImage
          style={{width: '100%', height: 250,backgroundColor: 'rgba(0,0,10,.7)'}}
          source={{uri: "http://192.168.1.115:8000/images_api/images/"+this.state.img_url}}
          resizeMode='contain'
      />
      <PricingCard
        color='#4f9deb'
        title={this.state.name}
        price={'$ '+this.state.price}
        info={['Descripción: '+this.state.decription, 'Categoría: '+this.state.category]}
        button={{ title: 'Comprar', icon: 'attach-money' }}
        onButtonPress={this.buyElement}
      /></View>
    </ScrollView>
      </View>
      
    );
  }
}
const styles = StyleSheet.create({

  outerContainer: {
    position: 'relative',
    backgroundColor: '#476dc5',
    borderBottomColor: '#476dc5',
    borderBottomWidth: 1,
    padding: 12,
    height: 53,
  },
  content:{
    flex:1,
    flexDirection:'column',
    alignItems:'center',
    justifyContent:'center'
},
}); 