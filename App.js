import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {StackNavigator} from 'react-navigation';
import login from './components/Login/login';
import Profile from './components/Profile';
import main from './components/main';
import productDetail from './components/second-screens/product_detail';
import interests from './components/tabs/interests';
import products from './components/tabs/products';
import info from './components/second-screens/info';

const Application=StackNavigator({
  Home:{screen: login},
  Profile:{screen:Profile},
  Main:{screen:main},
  productDetail:{screen:productDetail},  
  info:{screen:info}
  },{
    navigationOptions:{
      header:false,
    }
  }
  );

export default class App extends Component{
  
  render() {
    return (
      <Application />
    );
  }
}
