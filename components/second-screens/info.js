import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  KeyboardAvoidingView,
  TouchableOpacity,
  AsyncStorage,
  StatusBar,
  Dimensions,
  Platform, 
  PixelRatio,
  ScrollView
} from 'react-native';
import Toast from 'react-native-simple-toast';
import { NavigationActions } from 'react-navigation';
import { FormLabel, FormInput,Button, colors,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import ResponsiveImage from 'react-native-responsive-image';
import { responsiveFontSize } from 'react-native-responsive-dimensions';

export default class info extends Component{

  constructor(props){
    super(props)
    this.state={
      user:'',
      userPassword:'',
      status:true,      
      varLoad:false,
      backgroundColor:'#3E5EAB', 
      animated:false,
      translucent:false
    }
  }

  render() {
    let dom;
        
    return (
      <ScrollView style={styles.scroll} contentContainerStyle={styles.container}>
      <Header        
          statusBarProps={{
            backgroundColor:this.state.backgroundColor, 
            animated:this.state.animated,
            translucent:this.state.translucent}}
          outerContainerStyles={styles.outerContainer}          
        />
      <ResponsiveImage
            source={require('../../images/shop.jpg')}
            style={styles.backgroundImage}
          />
      <View style={styles.bodyContainer}>
        <View style={styles.logoContainer}>
          <ResponsiveImage
            source={require('../../images/logo.png')}
            style={styles.logo}
          />
          <View style={{ justifyContent: 'center',alignItems: 'center', paddingLeft:15,paddingRight:0}}>
          <Text style={styles.title}>APP STORE</Text>
          <Text style={styles.infoText}>Versión de la aplicación: 1.0</Text>
        <Text numberOfLines={10} style={styles.infoText}>La aplicación ha sido desarrollada con fines académicos, por lo que es una aplicación no oficial o registrada.
        La aplicación puede contener nombres reales de marcas o productos lo cual no indica que sean propiedad o esten operados por los creadores de esta aplicación o alguno de sus usuarios.
        </Text>
        </View>   
        </View>   
      </View>     
      </ScrollView>
    );
  }
}

const {
  width: SCREEN_WIDTH,
  height: SCREEN_HEIGHT,
} =Dimensions.get('window');

const scale = SCREEN_WIDTH / 320;

export function normalize(size) {
  if (Platform.OS === 'android') {
    return Math.round(PixelRatio.roundToNearestPixel(size))
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(size)) - 2
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',  
  },
  infoText: {
    color:'rgb(236, 240, 241)',
    fontWeight: 'bold',
    fontSize:responsiveFontSize(1.7)
  },
  backgroundImage:{
    position:'absolute',
    resizeMode: 'stretch',
    height:'100%',
    width:'100%'
  }
  ,
  logoContainer:{
    alignItems:'center',
    justifyContent:'center'
  },
  logo:{
    height:100,
    width:150
  },
  bodyContainer:{
    backgroundColor: 'rgba(44, 62, 80,0.86)',
    height:'100%',
    width:'100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textInput:{
    width:300,
    marginBottom:4
  },
  ButtonStyle:{
    marginTop:7, 
    borderRadius:5,
    backgroundColor:'rgb(22, 160, 133)',
  },
  title:{
    fontSize:30,
    color:'rgb(236, 240, 241)',
    fontWeight: 'bold'
  },
  scroll:{
    flex: 1,    
    height:'100%',
     
  },
  outerContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: '#fff',
    borderBottomColor: '#f2f2f2',
    borderBottomWidth: 1,
    padding: 15,
    height: 70,
  },
});
