import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  AsyncStorage,
  Text,
  CheckBox,
  View,
  Dimensions,
  Platform, 
  PixelRatio
} from 'react-native';
import {TabNavigator} from 'react-navigation';
import { responsiveFontSize } from 'react-native-responsive-dimensions';

import interests from './tabs/interests';
import products from './tabs/products';
import Profile from './../components/Profile';
import sales from './tabs/sales';

const {
  width: SCREEN_WIDTH,
  height: SCREEN_HEIGHT,
} =Dimensions.get('window');

const scale = SCREEN_WIDTH / 320;

export function normalize(size) {
  if (Platform.OS === 'android') {
    return Math.round(PixelRatio.roundToNearestPixel(size))
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(size)) - 2
  }
}

var myTabs = TabNavigator({
  Tab1:{screen:Profile},
  Tab2:{screen:products},
  Tab4:{screen:sales},
  Tab3:{screen:interests},  
},{
  animationEnabled:true,
  tabBarPosition:'bottom',
  swipeEnabled:true,
  tabBarOptions: {
                  showIcon: true,
                  labelStyle:{
                    fontSize:responsiveFontSize(1.2),
                  },
                  style: {
                    backgroundColor: '#476dc5',
                  }
                  }
});

export default myTabs;
