import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  AsyncStorage,
  Text,
  CheckBox,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  RefreshControl
} from 'react-native';
import NavigationBar from 'react-native-navbar';
import { Icon,Header, Button,Avatar,PricingCard, Card } from 'react-native-elements';
import { NavigationActions } from 'react-navigation';
import ResponsiveImage from 'react-native-responsive-image';


export default class Profile extends Component{

  static navigationOptions={
    tabBarLabel:'Perfil',
    tabBarIcon:({tintColor})=>(
      <Icon
        name='person'
        color='#ffffff' />
    ),
  }

  constructor(props){
    super(props)
    this.state={
      title:'User',
      id:'',
      total_sales:'',
      data:[],
      gender:'',
      navs:null,
      email:'',
      last_session:'',
      refreshing:false
    }
  }

  componentDidMount(){
    AsyncStorage.getItem("idUser").then((value) => {
    this.setState({"id": value});
    const {id}=this.state;
    fetch("http://192.168.1.115:8000/api/users?id="+id)
    .then((response)=>response.json())
    .then((responseJson)=>{
      this.setState({"data":responseJson});
      AsyncStorage.setItem("name",responseJson.users[0].name+"");
      AsyncStorage.setItem("gender",responseJson.users[0].gender+"");
      this.setState({"gender":responseJson.users[0].gender,
      "email":responseJson.users[0].email,'total_sales':responseJson.users[0].sales_total,
      'last_session':responseJson.last_session[0].last_session,
      "title": responseJson.users[0].name+" "+responseJson.users[0].first_name+" "+responseJson.users[0].last_name
    });
      //this.setState({"title": this.state.data.users[0].name+" "+responseJson.users[0].first_name+" "+responseJson.users[0].last_name});
    })
    .catch((error)=>{alert('Error de conexión');});
    });
  }

  closeSession=()=>{
    AsyncStorage.setItem("idUser","0");
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'Home'})
      ]
    })
    this.props.navigation.dispatch(resetAction)    
  }

  _onRefresh(){
    AsyncStorage.getItem("idUser").then((value) => {
      this.setState({"id": value});
      const {id}=this.state;
      fetch("http://192.168.1.115:8000/api/users?id="+id)
      .then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({"data":responseJson});
        AsyncStorage.setItem("name",responseJson.users[0].name+"");
        AsyncStorage.setItem("gender",responseJson.users[0].gender+"");
        this.setState({"gender":responseJson.users[0].gender,
        "email":responseJson.users[0].email,'total_sales':responseJson.users[0].sales_total,
        'last_session':responseJson.last_session[0].last_session});
        this.setState({"title": this.state.data.users[0].name+" "+responseJson.users[0].first_name+" "+responseJson.users[0].last_name});
      })
      .catch((error)=>{alert('Error de conexión');});
      });
  }


  render() {
    const {navigate} = this.props.navigation;
    let ac;
    if(this.state.gender==='M'){
      ac=<Text>MALE</Text>;
    }else{
      ac=<Text>FEMALE</Text>;
    }    
    return (
      <View>
      <Header
      centerComponent={{ text: 'BIENVENIDO', style: { color: '#fff' } }}
      leftComponent={<TouchableOpacity onPress={() => navigate('info')}><Icon name='info' color='#fff'/></TouchableOpacity>}
      statusBarProps={{backgroundColor:'#3E5EAB',
      animated:false,
      translucent:false}}
      outerContainerStyles={styles.outerContainer}   
    />
      <ScrollView
      refreshControl={
        <RefreshControl
        refreshing={this.state.refreshing}
        onRefresh={this._onRefresh.bind(this)}
        progressBackgroundColor='#476dc5'
        colors={['white']}
        />
        
      }
      >        
        <Card style={{width: 500, height: 500,backgroundColor: 'rgba(0,0,0,1)',borderTopLeftRadius: 10,
              borderTopRightRadius: 10,
              overflow: 'hidden'}}
              title="Mi información"
              >              
          <PricingCard
           color='#4f9deb'
           title={this.state.title}
           info={["Correo: "+this.state.email, 'Compras realizadas: '+this.state.total_sales,
            'Último inicio de sesión: '+this.state.last_session]}
           onButtonPress={this.closeSession}
           button={{title: 'CERRAR SESIÓN', icon: 'exit-to-app' }}
        />
              
        </Card>                                    
      </ScrollView>
      </View>
    );
  }

}
const styles = StyleSheet.create({
  
    outerContainer: {
      position: 'relative',
      backgroundColor: '#476dc5',
      borderBottomColor: '#476dc5',
      borderBottomWidth: 1,
      padding: 12,
      height: 53,
    },
  });