import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  KeyboardAvoidingView,
  TouchableOpacity,
  AsyncStorage,
  StatusBar,
  ScrollView
} from 'react-native';
import Toast from 'react-native-simple-toast';
import { NavigationActions } from 'react-navigation';
import { FormLabel, FormInput,Button, colors,Header } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import ResponsiveImage from 'react-native-responsive-image';
import PushNotification from 'react-native-push-notification';

export default class login extends Component{

  constructor(props){
    super(props)
    this.state={
      user:'',
      userPassword:'',
      status:false,      
      varLoad:false,
      backgroundColor:'#3E5EAB', 
      animated:false,
      translucent:false
    }
  }

  componentDidMount(){
    AsyncStorage.getItem("idUser").then((value) => {
      try{
      if(value!=null && value!=='0'){
        //this.setState({"backgroundColor":"#3E5EAB","animated":false,"translucent":false});
        this.props.navigation.dispatch(new NavigationActions.reset({
          index: 0,
          actions: [
           NavigationActions.navigate({ routeName: 'Main' })
           ]
         }));
      }else{
        this.setState({"status":false});
        this.setState({"backgroundColor":"rgba(0,0,0,0)","animated":true,"translucent":true});
      }}
      catch(error){
        alert('Error de conexión');
      }
    });
  }
  render() {
    let dom;
    
    if(this.state.status===false){
      dom=<View style={styles.bodyContainer}>
        <View style={styles.logoContainer}>
          <ResponsiveImage
            source={require('../../images/logo.png')}
            style={styles.logo}
          />
          <Text style={styles.title}>APP STORE</Text>
        </View>
        <View  >
      <TextField
        label='USUARIO'
        autoCapitalize="none" 
        keyboardType={'email-address'} 
        onChangeText={user=>this.setState({user})}
        inputContainerStyle	={styles.textInput}
        baseColor={'rgb(189, 195, 199)'}
        fontSize={19}
        textColor={'rgb(236, 240, 241)'}
      />
      <TextField
        label='CONTRASEÑA'
        autoCapitalize="none" 
        secureTextEntry={true} onChangeText={userPassword=>this.setState({userPassword})}
        inputContainerStyle	={styles.textInput}
        baseColor={'rgb(189, 195, 199)'}
        fontSize={19}
        textColor={'rgb(236, 240, 241)'}
      />
      <Button
        buttonStyle={styles.ButtonStyle}
        small
       // icon={{name: 'check', type: 'font-awesome'}}
        title={'Iniciar sesión'.toUpperCase()}
        onPress={this.userLogin}
        loading={this.state.varLoad} />
        </View>
      
      </View>;
    }
    return (
      <ScrollView style={styles.scroll} contentContainerStyle={styles.container}>
      <Header        
          statusBarProps={{
            backgroundColor:this.state.backgroundColor, 
            animated:this.state.animated,
            translucent:this.state.translucent}}
          outerContainerStyles={styles.outerContainer}          
        />
      <ResponsiveImage
            source={require('../../images/shop.jpg')}
            style={styles.backgroundImage}
          />
      {dom}  
    
      </ScrollView>
    );
  }

  userLogin=()=>{    
  const{user}=this.state;
  const{userPassword}=this.state;

  this.setState({'varLoad':true});
  fetch('http://192.168.1.115:8000/api/users/login',
  {
    method:'POST',
    mode: 'cors',
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      email: user,
      password: userPassword
    })
  }).then((response)=>response.json()).then((responseJson)=>{
    if(responseJson.status===true){
      AsyncStorage.setItem("idUser",responseJson.id+"");
      this.props.navigation.dispatch(new NavigationActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({ routeName: 'Main' })
    ]
      }));
      this.setState({'varLoad':false});

    }else{
      this.setState({'varLoad':false});
      Toast.showWithGravity('Verifique sus credenciales', Toast.SHORT, Toast.CENTER);
    }
  }).catch((error)=>{
    this.setState({'varLoad':false});
  alert('Error de conexión');
});
}

}


const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',  
  },
  backgroundImage:{
    position:'absolute',
    resizeMode: 'stretch',
    height:'100%',
    width:'100%'
  }
  ,
  logoContainer:{
    alignItems:'center',
    justifyContent:'center'
  },
  logo:{
    height:100,
    width:150
  },
  bodyContainer:{
    backgroundColor: 'rgba(44, 62, 80,0.86)',
    height:'100%',
    width:'100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textInput:{
    width:300,
    marginBottom:4
  },
  ButtonStyle:{
    marginTop:7, 
    borderRadius:5,
    backgroundColor:'rgb(22, 160, 133)',
  },
  title:{
    fontSize:30,
    color:'rgb(236, 240, 241)',
    fontWeight: 'bold'
  },
  scroll:{
    flex: 1,    
    height:'100%',
     
  },
  outerContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: '#fff',
    borderBottomColor: '#f2f2f2',
    borderBottomWidth: 1,
    padding: 15,
    height: 70,
  },
});
