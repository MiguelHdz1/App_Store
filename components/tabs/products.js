import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  AsyncStorage,
  Text,
  View,
  Image,
  ScrollView,
  FlatList,
  RefreshControl
} from 'react-native';
import { SearchBar, Icon, Card,Button } from 'react-native-elements';
import { NavigationActions } from 'react-navigation';

export default class products extends Component{

  static navigationOptions={
    tabBarLabel:'Productos',
    tabBarIcon:({tintColor})=>(
      <Icon
        name='store'
        color='#ffffff' />
    ),
  }
  constructor(props){
    super(props)
    this.state={
      id:'',
      data:null,
      searchText:'',
      refreshing:false,
      searchTxt:''
    }
  }

  componentDidMount(){
    AsyncStorage.getItem("idUser").then((value) => {
    this.setState({"id": value});
    const {id}=this.state;
    fetch("http://192.168.1.115:8000/api/products/"+this.state.id)
    .then((response)=>response.json())
    .then((responseJson)=>{
      this.setState({"data":responseJson.productos});
      //alert(this.state.data.productos[5].name);
    })
    .catch((error)=>{alert('Error de conexión');});
    });
  }

  _onRefresh(){
    AsyncStorage.getItem("idUser").then((value) => {      
      this.setState({"id": value,"refreshing":true});
      const {id}=this.state;
      fetch("http://192.168.1.115:8000/api/products/"+this.state.id)
      .then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({"data":responseJson.productos,"refreshing":false});
        //alert(this.state.data.productos[5].name);
      })
      .catch((error)=>{alert('Error de conexión');});
      });
  }

  filterSearch(searchText){
    this.setState({"data":null,searchTxt:searchText});    
    fetch("http://192.168.1.115:8000/api/products/"+this.state.id+"?name="+searchText)
    .then((response)=>response.json())
    .then((responseJson)=>{
      this.setState({"data":responseJson.productos});
    })
    .catch((error)=>{alert('Error de conexión');}); 
  }


  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={{paddingBottom: 64}} >
      <SearchBar     
        lightTheme   
        onChangeText={(searchText)=>this.filterSearch(searchText)}
        containerStyle={{backgroundColor: '#476dc5'}}        
        inputStyle={{backgroundColor: '#476dc5',color:'#fafafa'}}
        placeholderTextColor='#C0C0C0'
        clearIcon={{color:'white'}}
        placeholder='Buscar...' 
        onClearText={this.componentDidMount}    
        onBlur={()=>this.filterSearch(this.state.searchTxt)}    
        />        
        <FlatList

          data={this.state.data}
          keyExtractor={(x,i)=>i}
          renderItem={
            ({item})=>
            <Card
              containerStyle={{elevation: 4}}
              featuredTitle={item.name}
              image={{ uri: "http://192.168.1.115:8000/images_api/images/"+item.img_url }}>
              <Text style={{fontSize: 18, fontWeight:'bold'}}>{item.name}</Text>
                <Text>{"Precio: $ "+item.price+" MXN"}</Text>
                <Text>
                  {item.decription}
                </Text>
                <Text style={{marginBottom: 10}}>
                  {"Núm. de busquedas: "+item.rating_count}
                </Text>
                <Button
                  icon={{name: 'code'}}
                  backgroundColor='#476dc5'
                  fontFamily='Lato'
                  onPress={() => navigate('productDetail', {idProduct:""+item.id_product})}
                  buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}
                  title='VER DETALLES'/>
            </Card>
          }
          refreshControl={
            <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh.bind(this)}
            progressBackgroundColor='#476dc5'
            colors={['white']}
            />
          }
        />
      </View>
    );
  }
}
