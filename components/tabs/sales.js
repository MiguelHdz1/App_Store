import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  AsyncStorage,
  Text,
  CheckBox,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  RefreshControl  
} from 'react-native';
import NavigationBar from 'react-native-navbar';
import { Icon,Header, Button,Avatar,PricingCard, Card,ListItem } from 'react-native-elements';
import { NavigationActions } from 'react-navigation';
import ResponsiveImage from 'react-native-responsive-image';


export default class sales extends Component{

  static navigationOptions={
    tabBarLabel:'Compras',
    tabBarIcon:({tintColor})=>(
      <Icon
        name='shopping-cart'
        color='#ffffff' />
    ),
  }

  constructor(props){
    super(props)
    this.state={
      id:'',
      data:[],
      refreshing:false
    }
  }

  /*componentDidMount=()=>{    
    AsyncStorage.getItem("idUser").then((value) => {
      this.setState({"id": value});
      const {id}=this.state;
      fetch("http://192.168.1.115:8000/api/purchases/"+value)
      .then((response)=>response.json())
      .then((responseJson)=>{        
        this.setState({data:responseJson.purchases});
      })
      .catch((error)=>{                
      });
    });
  }*/

  componentDidMount=()=>{    
    AsyncStorage.getItem("idUser").then((value) => {
      this.setState({"id": value});
      const {id}=this.state;
      fetch("http://192.168.1.115:8000/api/purchases/"+id)
      .then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({"data":responseJson.purchases});        
      })
      .catch((error)=>{alert('Error de conexión');});
      });
  }

  _onRefresh(){
    AsyncStorage.getItem("idUser").then((value) => {
      this.setState({"id": value,"refreshing":true});      
      const {id}=this.state;
      fetch("http://192.168.1.115:8000/api/purchases/"+value)
      .then((response)=>response.json())
      .then((responseJson)=>{
        this.setState({data:responseJson.purchases,"refreshing":false});        
      })
      .catch((error)=>{
        this.setState({"refreshing":false});
        alert('Error de conexión');
      });
      });
  }

  render() {    
    const {navigate} = this.props.navigation;
    return (
      <View style={{paddingBottom: 55}}>
        <Header
            leftComponent={<TouchableOpacity onPress={() => navigate('info')}><Icon name='info' color='#fff'/></TouchableOpacity>}
            rightComponent={<TouchableOpacity onPress={this.componentDidMount}><Icon name='refresh' color='#fff'/></TouchableOpacity>}
            centerComponent={{ text: 'MIS COMPRAS', style: { color: '#fff' } }}
            statusBarProps={{backgroundColor:'#3E5EAB',
            animated:false,
            translucent:false}}
            outerContainerStyles={styles.outerContainer}  
        />
        <ScrollView
        refreshControl={
          <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh.bind(this)}
          progressBackgroundColor='#476dc5'
          colors={['white']}
          />
        }
        >          
        {
            this.state.data.map((u, i) => {
              return (
                <ListItem                
                key={i}
                chevronColor={'green'}
                roundAvatar
                hideChevron={true}
                title={u.name}
                subtitleNumberOfLines={2}
                badge={{ value: '$ '+u.price, textStyle: { color: 'orange' } }}                  
                subtitle={'Compra realizada el '+u.date}
                avatar={{uri:'http://192.168.1.115:8000/images_api/images/'+u.img_url}}
                />                         
              );
            })
          }
        </ScrollView>
      </View>
    );
  }

}
const styles = StyleSheet.create({
  
    outerContainer: {
      position: 'relative',
      backgroundColor: '#476dc5',
      borderBottomColor: '#476dc5',
      borderBottomWidth: 1,
      padding: 12,
      height: 53,
    },
  });